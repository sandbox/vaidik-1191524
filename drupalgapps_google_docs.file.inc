<?php

function drupalgapps_google_docs_field_formatter_info() {
	$return_array = array();

	if (variable_get('drupalgapps_google_docs_add_formatter_file_default', 0)) {
		$return_array += array(
	    'file_google_docs_generic' => array(
      	'label' => t('Generic file link with link to Google Docs Viewer'),
    	  'field types' => array('file'),
  	  ),
	  );
	}

	if (variable_get('drupalgapps_google_docs_add_formatter_file_table', 0)) {
    $return_array += array(
			'file_google_docs_table' => array(
    	  'label' => t('Table of files with link to Google Docs Viewer'),
  	    'field types' => array('file'),
	    ),
    );
  }

	return $return_array;
}

function drupalgapps_google_docs_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
	$element = array();

  switch ($display['type']) {
    case 'file_google_docs_generic':
			foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'file_link',
          '#file' => (object) $item,
					'#suffix' => ' | ' . l(t('Open in Google Docs Viewer'), 'http://docs.google.com/viewer?url=' . urlencode(empty($item['uri']) ? '' : file_create_url($item['uri'])), array('attributes' => array('target' => '_blank'))),
        );
      }
			break;
		case 'file_google_docs_table':
			$element[0] = array(
        '#theme' => 'drupalgapps_google_docs_file_formatter_table',
        '#items' => $items,
      );
			break;
	}

	return $element;
}

/**
 * Returns HTML for a file attachments table.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of file attachments.
 *
 * @ingroup themeable
 */
function theme_drupalgapps_google_docs_file_formatter_table($variables) {
  $header = array(t('Attachment'), t('Size'), t('View'));
  $rows = array();
  foreach ($variables['items'] as $delta => $item) {
    $rows[] = array(
      theme('file_link', array('file' => (object) $item)),
      format_size($item['filesize']),
			l(t('Open in Google Docs Viewer'), 'http://docs.google.com/viewer?url=' . urlencode(empty($item['uri']) ? '' : file_create_url($item['uri'])), array('attributes' => array('target' => '_blank'))),
    );
  }

  return empty($rows) ? '' : theme('table', array('header' => $header, 'rows' => $rows));
}
