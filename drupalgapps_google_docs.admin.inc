<?php

function drupalgapps_google_docs_admin_form() {
	global $user;

	$form = array();

	/**
	 * @TODO: Design options have to be given. Add dragable table.
	 */

	$form['configuration'] = array(
		'#type' => 'fieldset',
		'#title' => t('Configuration'),
		'#collapsed' => FALSE,
		'#collapsible' => TRUE,
	);

	$form['configuration']['drupalgapps_google_docs_show_user_page'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show the default <em>User Page</em> for letting users view their Google Docs files'),
		'#description' => t('Selecting this option will show a user page at the following ') . l(t('link'), 'user/' . $user->uid . '/drupalgapps_google_docs/list_docs') . '.',
		'#default_value' => variable_get('drupalgapps_google_docs_show_user_page', 1),
	);

	$form['configuration']['file_upload'] = array(
		'#type' => 'fieldset',
		'#title' => t('Upload Settings'),
		'#description' => t('Configuration for temporary files created while uploading your files to Google Docs.'),
		'#collapsed' => FALSE,
		'#collapsible' => TRUE,
	);

	$form['configuration']['file_upload']['drupalgapps_google_docs_upload_temp_stream'] = array(
		'#type' => 'select',
		'#title' => t('Stream for temporary files'),
		'#description' => t('Select a stream for storing temporary files created while uploading your files to Google Docs. !stream_p points to !locat_p directory of your Drupal installation\'s root and !stream_t points to !locat_t directory.', array('!stream_p' => 'Public', '!stream_t' => 'Temporary', '!locat_p' => 'sites/default/files/', '!locat_t' => '/tmp/')),
		'#options' => array(
			'public://' => t('!stream', array('!stream' => 'Public')),
			'temporary://' => t('!stream', array('!stream' => 'Temporary')),
		),
		'#default_value' => variable_get('drupalgapps_google_docs_upload_temp_stream', 'public://'),
	);

	$form['configuration']['file_upload']['drupalgapps_google_docs_upload_temp_directory'] = array(
		'#type' => 'textfield',
		'#title' => t('Directory for temporary files'),
		'#description' => t('Name of the directory in the selected stream. This directory will be the location where temporary files will be created while uploading your file to Google Docs. This directory will reside in your stream.'),
		'#default_value' => variable_get('drupalgapps_google_docs_upload_temp_directory', ''),
	);

	$form['configuration']['file_field_display'] = array(
		'#type' => 'fieldset',
    '#title' => t('File Field Settings'),
		'#description' => t('Configuration for managing display of Drupal\'s File field.'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
	);

	$form['configuration']['file_field_display']['info'] = array(
		'#markup' => '<p>' . t('DrupalGapps Google Docs module provides two formatters for displaying your file fields input with a link to directly open your files in Google Docs Viewer.') . '</p>',
	);

	$form['configuration']['file_field_display']['drupalgapps_google_docs_add_formatter_file_default'] = array(
		'#type' => 'checkbox',
		'#title' => t('Add new <em>Generic File</em> formatter for File field.'),
		'#description' => t('Selecting this option will create a new File field formatter which will append a direct link to open files attached to nodes. This formatter will be similar to <em>Generic File</em> formatter for File field.'),
		'#default_value' => variable_get('drupalgapps_google_docs_add_formatter_file_default', 0),
	);

	$form['configuration']['file_field_display']['drupalgapps_google_docs_add_formatter_file_table'] = array(
		'#type' => 'checkbox',
		'#title' => t('Add new <em>Table of Files</em> formatter for File field.'),
		'#description' => t('Selecting this option will create a new File field formatter which will append a direct link to open files attached to nodes. This formatter will be similar to <em>Table of Files</em> formatter for File field.'),
		'#default_value' => variable_get('drupalgapps_google_docs_add_formatter_file_table', 0),
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
	);

	return $form;
}

function drupalgapps_google_docs_admin_form_submit($form, &$form_state) {
	variable_set('drupalgapps_google_docs_show_user_page', $form_state['values']['drupalgapps_google_docs_show_user_page']);

	variable_set('drupalgapps_google_docs_add_formatter_file_default', $form_state['values']['drupalgapps_google_docs_add_formatter_file_default']);
	variable_set('drupalgapps_google_docs_add_formatter_file_table', $form_state['values']['drupalgapps_google_docs_add_formatter_file_table']);

	variable_set('drupalgapps_google_docs_upload_temp_stream', $form_state['values']['drupalgapps_google_docs_upload_temp_stream']);
	variable_set('drupalgapps_google_docs_upload_temp_directory', $form_state['values']['drupalgapps_google_docs_upload_temp_directory']);

	include_once './includes/bootstrap.inc';
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  drupal_flush_all_caches();
}
